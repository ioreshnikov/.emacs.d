;; Package repository
(require 'package)
(setq package-archives nil)
(add-to-list 'package-archives '("elpa"      . "https://elpa.gnu.org/elpa/"))
(add-to-list 'package-archives '("marmalade" . "https://marmalade-repo.org/packages/"))
(add-to-list 'package-archives '("melpa"     . "https://melpa.org/packages/"))
(package-initialize)

;; Additional modes
(load "~/.emacs.d/contrib/focus-mode.el")

;; Settings
(load "~/.emacs.d/functions.el")
(load "~/.emacs.d/general.el")
(load "~/.emacs.d/keybindings.el")
(load "~/.emacs.d/appearance.el")
(load "~/.emacs.d/eshell.el")
(load "~/.emacs.d/magit.el")
(load "~/.emacs.d/company.el")
(load "~/.emacs.d/haskell.el")
(load "~/.emacs.d/python.el")
(load "~/.emacs.d/org.el")
(load "~/.emacs.d/flycheck.el")
(load "~/.emacs.d/tex.el")
(load "~/.emacs.d/markdown.el")
(load "~/.emacs.d/maxima.el")
(load "~/.emacs.d/fortran.el")
(load "~/.emacs.d/restclient.el")
