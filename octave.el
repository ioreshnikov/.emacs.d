(require 'octave-mod)
(require 'octave-inf)

(add-to-list 'auto-mode-alist '("\\.m\\'" . octave-mode))

(setq inferior-octave-prompt-read-only t)
(setq octave-blink-matching-block nil)

(defun octave-send-buffer ()
  (interactive)
  (octave-send-region (point-min) (point-max)))

(define-key octave-mode-map (kbd "C-c C-l") 'octave-send-buffer)

;; FFS!
(add-hook 'octave-mode-hook (lambda () (setq comment-start "%%"
                                             comment-end   "")))
