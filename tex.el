(require 'tex)
(require 'reftex)

(add-hook 'TeX-mode-hook 'flyspell-mode)
(add-hook 'TeX-mode-hook 'reftex-mode)
(add-hook 'TeX-mode-hook 'TeX-PDF-mode)
(add-hook 'TeX-mode-hook 'rainbow-delimiters-mode)
(add-hook 'TeX-mode-hook 'visual-line-mode)

(setq reftex-plug-into-AUCTeX t)
(setq TeX-parse-self t)
(setq-default TeX-master nil)

(setq TeX-open-quote  "<<")
(setq TeX-close-quote ">>")

(setq TeX-electric-sub-and-superscript t)

(setq font-latex-fontify-script nil)

(setq TeX-show-compilation nil)

(setq preview-scale-function 1.5)
(setq preview-gs-options
      '("-q" "-dNOSAFER" "-dNOPAUSE" "-DNOPLATFONTS"
        "-dPrinted" "-dTextAlphaBits=4" "-dGraphicsAlphaBits=4"))

(setq reftex-label-alist '(AMSTeX))
