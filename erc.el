(setq erc-hide-list '("JOIN" "PART" "QUIT"))
(setq erc-fill-function 'erc-fill-static)
(setq erc-fill-static-center 16)


(defconst erc-nick-num-faces 7
  "Number of faces to highlight nicks in irc chats")

(defface erc-nick-1-face
  '((t (:foreground "red" :background nil)))
  "Face to highlight nicks in irc chats"
  :group 'erc-faces)

(defface erc-nick-2-face
  '((t (:foreground "orange" :background nil)))
  "Face to highlight nicks in irc chats"
  :group 'erc-faces)

(defface erc-nick-3-face
  '((t (:foreground "yellow" :background nil)))
  "Face to highlight nicks in irc chats"
  :group 'erc-faces)

(defface erc-nick-4-face
  '((t (:foreground "green" :background nil)))
  "Face to highlight nicks in irc chats"
  :group 'erc-faces)

(defface erc-nick-5-face
  '((t (:foreground "blue" :background nil)))
  "Face to highlight nicks in irc chats"
  :group 'erc-faces)

(defface erc-nick-6-face
  '((t (:foreground "cyan" :background nil)))
  "Face to highlight nicks in irc chats"
  :group 'erc-faces)

(defface erc-nick-7-face
  '((t (:foreground "violet" :background nil)))
  "Face to highlight nicks in irc chats"
  :group 'erc-faces)


(defun erc-get-nick-face (nick)
  (let (number face-name)
    (setq number
          (+ 1 (mod (string-to-number (md5 (downcase nick)))
                    erc-nick-num-faces)))
    (setq face-name (format "erc-nick-%d-face" number))
    (intern face-name)))

(defun erc-fontify-nicks ()
  (interactive)
  (goto-char (point-min))
  (if (looking-at "\\s-*<\\([^ >]*\\)>\\s-*\\(?:\\([^ :]*\\):\\)?")
      (let ((from-nick (match-string 1))
            (to-nick   (match-string 2)))
        (put-text-property (match-beginning 1) (match-end 1)
                           'face (erc-get-nick-face from-nick))
        (if to-nick
            (put-text-property (match-beginning 2) (match-end 2)
                               'face (erc-get-nick-face to-nick))))))

(add-hook 'erc-insert-modify-hook 'erc-fontify-nicks)
