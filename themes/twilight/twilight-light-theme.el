(require  'twilight-definitions)
(deftheme  twilight-light)
(make-twilight-theme 'light 'twilight-light)
(provide  'twilight-light)
