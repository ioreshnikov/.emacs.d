(defun make-twilight-theme (variant theme)

  (let (gray-1 gray-1bg
        gray-2
        gray-3 gray-3bg
        gray-4
        gray-5

        red-1    red-1bg
        red-2    red-2bg
        brown-1  brown-1bg
        orange-1 orange-1bg
        green-1  green-1bg
        blue-1   blue-1bg
        blue-2   blue-2bg
        blue-3   blue-3bg
        blue-4   blue-4bg
        purple-1 purple-1bg

        background
        foreground
        region
        hlline
        cursor
        comment)

    (cond ((eq   variant    'light)
           (setq ;; Grayscale
                 gray-1 "#a49da5" gray-1bg "#f7f7f7"
                 gray-2 "#d9d9d9"
                 gray-3 "#b3adb4" gray-3bg "#eaeaea"
                 gray-4 "#c8c8c8"
                 gray-5 "#efefef"

                 ;; Colors
                 red-1    "#d15120" red-1bg    "#fdf2ed"
                 red-2    "#b23f1e" red-2bg    "#fcf3f1"
                 brown-1  "#9f621d" brown-1bg  "#fdf2ed"
                 orange-1 "#cf7900" orange-1bg "#fdf9f2"
                 yellow-1 "#d2ad00" yellow-1bg "#faf7e7"
                 green-1  "#5f9411" green-1bg  "#eff8e9"
                 blue-1   "#6b82a7" blue-1bg   "#f1f4f8"
                 blue-2   "#417589" blue-2bg   "#e3f4ff"
                 purple-1 "#a66bab" purple-1bg "#f8f1f8"

                 ;; Special
                 background "#ffffff"
                 foreground "#505050"
                 region     "#c7e1f2"
                 hlline     "#f5f5f5"
                 ;; cursor     "#b4b4b4"
                 cursor     "#2a3441"

                 ;; Rest
                 highlight  blue-2   highlight-bg  blue-2bg
                 vertical   gray-4
                 modeline   blue-1   modeline-bg   blue-2bg
                 modelineia gray-1   modelineia-bg gray-3bg
                 comment    gray-1   comment-bg    gray-1bg
                 pmatch     orange-1 pmatch-bg     orange-1bg
                 pmismatch  red-2bg  pmismatch-bg  red-2
                 ))

          ((eq   variant    'dark)
           (setq ;; Grayscale
                 gray-1 "#878289" gray-1bg "#181d23"
                 gray-2 "#2a3441"
                 gray-3 "#b3adb4" gray-3bg "#0e1116"
                 gray-4 "#1f2730"
                 gray-5 "#242d38"

                 ;; Colors
                 red-1    "#d15120" red-1bg    "#2a1f1f"
                 red-2    "#b23f1e" red-2bg    "#251c1e"
                 brown-1  "#9f621d" brown-1bg  "#2a1f1f"
                 orange-1 "#d97a35" orange-1bg "#272122"
                 yellow-1 "#deae3e" yellow-1bg "#2a2921"
                 green-1  "#81af34" green-1bg  "#1a2321"
                 blue-1   "#7e8fc9" blue-1bg   "#1e252f"
                 blue-2   "#417598" blue-2bg   "#1b333e"
                 blue-3   "#00959e" blue-3bg   "#132228"
                 blue-4   "#365e7a" blue-4bg   "#172028"
                 purple-1 "#a878b5" purple-1bg "#25222f"

                 ;; Special
                 background "#14191f"
                 foreground "#dcdddd"
                 region     "#313c4d"
                 hlline     "#11151a"
                 cursor     "#b4b4b4"


                 ;; Rest
                 highlight  blue-3    highlight-bg  blue-3bg
                 vertical   gray-5
                 modeline   blue-1    modeline-bg   blue-2bg
                 modelineia blue-4    modelineia-bg gray-5
                 comment    "#716d73" comment-bg    gray-1bg
                 pmatch     blue-2    pmatch-bg     blue-2bg
                 pmismatch  red-1     pmismatch-bg  red-1bg
                 )))

    (custom-theme-set-faces
     theme

     ;; Defaults and UI
     `(default ((t (:foreground ,foreground :background ,background))))
     `(cursor  ((t (:foreground nil         :background ,cursor))))
     `(region  ((t (:foreground nil         :background ,region))))
     ;; `(fringe  ((t (:foreground nil         :background ,gray-1bg))))
     `(fringe  ((t (:foreground ,foreground :background ,background))))
     `(match               ((t (:foreground ,highlight  :background ,highlight-bg))))
     `(highlight           ((t (:foreground ,highlight  :background ,highlight-bg))))
     `(secondary-selection ((t (:foreground ,highlight  :background ,highlight-bg))))
     `(vertical-border     ((t (:foreground ,vertical   :background ,background))))
     `(header-line         ((t (:foreground ,comment    :background ,comment-bg))))
     `(mode-line           ((t (:foreground ,modeline   :background ,modeline-bg   :box nil))))
     `(mode-line-inactive  ((t (:foreground ,modelineia :background ,modelineia-bg :box nil))))
     `(minibuffer-prompt   ((t (:foreground ,yellow-1   :background ,yellow-1bg))))

     ;; Markup
     `(bold      ((t (:foreground nil :bold t))))
     `(italic    ((t (:foreground nil :italic t :underline nil))))
     `(underline ((t (:foreground nil :underline t))))
     `(escape-glyph ((t (:foreground ,gray-3))))
     `(link         ((t (:foreground ,blue-1   :background ,blue-1bg   :underline t))))
     `(link-visited ((t (:foreground ,purple-1 :backgroudn ,purple-1bg :underline t))))

     ;; Isearch
     `(isearch        ((t (:foreground nil      :background ,region))))
     `(lazy-highlight ((t (:foreground nil      :background ,gray-2))))
     `(isearch-fail   ((t (:foreground ,red-1bg :background ,red-1 :bold t))))

     ;; Linum
     `(linum   ((t (:foreground ,gray-2 :background ,gray-1bg))))

     ;; HL-Line
     `(hl-line ((t (:foreground nil :background ,hlline :inherit nil))))

     ;; Font-lock
     `(font-lock-builtin-face       ((t (:foreground ,yellow-1 :background ,yellow-1bg :bold nil))))
     `(font-lock-constant-face      ((t (:foreground ,purple-1 :background ,purple-1bg))))
     `(font-lock-comment-face       ((t (:foreground ,comment  :background ,comment-bg :italic t))))
     `(font-lock-doc-face           ((t (:foreground ,gray-1   :background ,gray-1bg))))
     `(font-lock-function-name-face ((t (:foreground ,red-1    :background ,red-1bg))))
     `(font-lock-keyword-face       ((t (:foreground ,orange-1 :background ,orange-1bg :bold nil))))
     `(font-lock-preprocessor-face  ((t (:foreground ,orange-1 :background ,orange-1bg))))
     `(font-lock-string-face        ((t (:foreground ,green-1  :background ,green-1bg))))
     `(font-lock-type-face          ((t (:foreground ,red-2    :background ,red-2bg))))
     `(font-lock-variable-name-face ((t (:foreground ,blue-1   :background ,blue-1bg))))
     `(font-lock-warning-face       ((t (:foreground ,red-2    :background ,red-2bg))))
     `(font-lock-negation-char-face ((t (:foreground ,yellow-1 :background ,yellow-1bg))))
     `(font-lock-regexp-grouping-backslash ((t (:foreground ,yellow-1 :background ,yellow-1bg))))
     `(font-lock-regexp-grouping-construct ((t (:foreground ,orange-1 :background ,orange-1bg))))

     ;; LaTeX
     `(font-latex-math-face    ((t (:foreground ,yellow-1   :background ,yellow-1bg))))
     `(font-latex-warning-face ((t (:foreground ,orange-1   :background ,orange-1bg))))
     `(font-latex-bold-face    ((t (:foreground ,foreground :bold t))))
     `(font-latex-italic-face  ((t (:foreground ,foreground :italic t))))
     `(font-latex-string-face  ((t (:foreground ,green-1    :background ,green-1bg))))
     `(font-latex-sectioning-1-face ((t (:foreground ,foreground :bold t :height 1.0))))
     `(font-latex-sectioning-2-face ((t (:foreground ,foreground :bold t :height 1.0))))
     `(font-latex-sectioning-3-face ((t (:foreground ,foreground :bold t :height 1.0))))
     `(font-latex-sectioning-4-face ((t (:foreground ,foreground :bold t :height 1.0))))
     `(font-latex-sectioning-5-face ((t (:foreground ,foreground :bold t :height 1.0 :inherit nil))))
     `(font-latex-slide-title-face  ((t (:foreground ,foreground :bold t :height 1.0 :inherit nil))))
     `(font-latex-verbatim-face ((t (:foreground ,blue-1 :background ,blue-1bg))))

     ;; Cperl
     `(cperl-hash-face           ((t (:foreground ,purple-1 :background ,purple-1bg))))
     `(cperl-array-face          ((t (:foreground ,green-1  :background ,green-1bg))))
     `(cperl-nonoverridable-face ((t (:foreground ,yellow-1 :background ,yellow-1bg))))

     ;; Sh mode
     `(sh-quoted-exec ((t (:foreground ,yellow-1 :background ,yellow-1bg))))
     `(sh-heredoc     ((t (:foreground ,green-1  :background ,green-1bg))))

     ;; Parens
     ;; `(show-paren-match-face    ((t (:foreground ,pmatch    :background ,pmatch-bg    :bold nil))))
     ;; `(show-paren-mismatch-face ((t (:foreground ,pmismatch :background ,pmismatch-bg :bold nil))))
     `(show-paren-match-face    ((t (:foreground ,foreground :background ,background :bold nil :underline t))))
     `(show-paren-mismatch-face ((t (:foreground ,foreground :background ,background :bold nil :underline t))))

     ;; IDO
     `(ido-subdir      ((t (:foreground ,purple-1   :background ,purple-1bg))))
     `(ido-first-match ((t (:foreground ,foreground :background ,background :bold t))))
     `(ido-only-match  ((t (:foreground ,green-1    :background ,green-1bg))))

     ;; Eshell
     `(eshell-prompt        ((t (:foreground ,blue-2 :background ,blue-2bg :bold nil))))
     `(eshell-ls-directory  ((t (:foreground ,purple-1 :background ,purple-1bg :bold nil))))
     `(eshell-ls-readonly   ((t (:foreground ,red-1    :background ,red-1bg))))
     `(eshell-ls-unreadable ((t (:foreground ,red-2    :background ,red-2bg))))
     `(eshell-ls-archive    ((t (:foreground ,green-1  :background ,green-1bg))))
     `(eshell-ls-symlink    ((t (:foreground ,blue-1   :background ,blue-1bg :underline t))))
     `(eshell-ls-executable ((t (:foreground ,yellow-1 :background ,yellow-1bg))))
     `(eshell-ls-product    ((t (:foreground ,blue-2   :background ,blue-2bg))))
     `(eshell-ls-backup     ((t (:foreground ,gray-3))))

     ;; Flyspell
     `(flyspell-incorrect ((t (:foreground nil :inherit nil :underline (:color ,red-1)))))
     `(flyspell-duplicate ((t (:foreground nil :inherit nil :underline (:color ,orange-1)))))

     ;; Flycheck
     `(flycheck-fringe-info ((t (:weight normal))))
     `(flycheck-fringe-error ((t (:foreground ,red-1  :background ,red-1bg :weight normal))))
     `(flycheck-fringe-warning ((t (:foreground ,yellow-1 :background ,yellow-1bg :weight normal))))
     `(flycheck-error ((t (:underline (:color ,red-1 :style line)))))
     `(flycheck-warning ((t (:underline (:color ,orange-1 :style line)))))

     ;; Magit
     `(magit-section-heading ((t (:foreground ,foreground :background ,background :bold t))))
     `(magit-section-highlight ((t (:foreground nil :background nil :inherit nil))))
     `(magit-hash ((t (:foreground ,yellow-1 :background ,yellow-1bg))))
     `(magit-branch-current ((t (:foreground ,green-1    :background ,green-1bg  :box nil))))
     `(magit-branch-local   ((t (:foreground ,orange-1   :background ,orange-1bg :box nil))))
     `(magit-branch-remote  ((t (:foreground ,blue-1     :background ,blue-1bg   :box nil))))
     `(magit-branch-default ((t (:foreground ,foreground :background ,background :box nil))))
     `(magit-tag ((t (:foreground ,red-1 :background ,red-1bg))))

     `(magit-key-mode-header-face ((t (:foreground ,foreground :background ,background :bold t))))
     `(magit-key-mode-button-face ((t (:foreground ,highlight  :background ,highlight-bg))))

     `(magit-diff-added   ((t (:foreground ,green-1  :background ,green-1bg))))
     `(magit-diff-removed ((t (:foreground ,red-1    :background ,red-1bg))))
     `(magit-diff-added-highlight   ((t (:foreground ,green-1  :background ,green-1bg))))
     `(magit-diff-removed-highlight ((t (:foreground ,red-1    :background ,red-1bg))))
     `(magit-diffstat-added   ((t (:foreground ,green-1  :background ,green-1bg))))
     `(magit-diffstat-removed ((t (:foreground ,red-1    :background ,red-1bg))))
     `(magit-diff-hunk-heading           ((t (:foreground ,comment    :background ,comment-bg))))
     `(magit-diff-hunk-heading-highlight ((t (:foreground nil         :background ,comment-bg))))
     `(magit-diff-context-highlight      ((t (:foreground ,foreground :background nil))))
     `(magit-diff-file-heading           ((t (:foreground ,foreground :background ,hlline))))
     `(magit-diff-file-heading-highlight ((t (:foreground nil         :background nil :inherit nil))))
     `(magit-diff-file-heading-selection ((t (:foreground ,foreground :background ,region :inherit nil))))

     `(magit-log-hash   ((t (:foreground ,yellow-1 :background ,yellow-1bg))))
     `(magit-log-author ((t (:foreground ,blue-1   :background ,blue-1bg))))
     `(magit-log-date   ((t (:foreground ,purple-1 :background ,purple-1bg))))
     `(magit-log-head-label-head    ((t (:foreground ,green-1    :background ,green-1bg  :box nil))))
     `(magit-log-head-label-local   ((t (:foreground ,orange-1   :background ,orange-1bg :box nil))))
     `(magit-log-head-label-remote  ((t (:foreground ,blue-1     :background ,blue-1bg   :box nil))))
     `(magit-log-head-label-default ((t (:foreground ,foreground :background ,background :box nil))))
     `(magit-log-head-label-tags    ((t (:foreground ,red-1      :background ,red-1bg    :box nil))))

     `(git-commit-summary  ((t (:foreground ,foreground :background ,background :bold t))))
     `(git-commit-branch   ((t (:foreground ,orange-1   :background ,orange-1bg))))
     `(git-commit-comment-heading ((t (:foreground ,foreground :background ,background :bold t))))
     `(git-commit-comment-action  ((t (:foreground ,yellow-1   :background ,yellow-1bg))))
     `(git-commit-comment-file ((t (:foreground ,foreground :background ,background))))

     ;; Woman
     `(woman-bold    ((t (:foreground ,foreground :bold t))))
     `(woman-italic  ((t (:foreground ,foreground :italic t))))
     `(woman-unknown ((t (:foreground ,purple-1   :background ,purple-1bg :bold nil))))

     ;; Info
     `(info-title-1     ((t (:foreground ,foreground :height 2.25 :bold nil))))
     `(info-title-2     ((t (:foreground ,foreground :height 2.25 :bold nil))))
     `(info-title-3     ((t (:foreground ,foreground :height 1.50 :bold t))))
     `(info-title-4     ((t (:foreground ,foreground :height 1.50 :bold t))))
     `(info-menu-header ((t (:foreground ,foreground :bold t))))
     `(info-menu-star   ((t (:foreground ,foreground))))
     `(info-xref        ((t (:foreground ,blue-1   :background ,blue-1bg   :underline t))))
     `(info-header-node ((t (:foreground ,purple-1 :background ,purple-1bg :underline t))))

     ;; Org mode
     `(org-document-title ((t (:foreground ,foreground :background ,gray-3bg :bold t :height 1.0))))
     `(org-document-info         ((t (:foreground ,yellow-1 :background ,yellow-1bg))))
     `(org-document-info-keyword ((t (:foreground ,comment  :background ,comment-bg))))
     `(org-level-1 ((t (:foreground ,foreground :bold t :italic nil :inherit nil))))
     `(org-level-2 ((t (:foreground ,foreground :bold t :italic nil :inherit nil))))
     `(org-level-3 ((t (:foreground ,foreground :bold t :italic nil :inherit nil))))
     `(org-level-4 ((t (:foreground ,foreground :bold t :italic nil :inherit nil))))
     `(org-level-5 ((t (:foreground ,foreground :bold t :italic nil :inherit nil))))
     `(org-level-6 ((t (:foreground ,foreground :bold t :italic nil :inherit nil))))
     `(org-level-7 ((t (:foreground ,foreground :bold t :italic nil :inherit nil))))
     `(org-level-8 ((t (:foreground ,foreground :bold t :italic nil :inherit nil))))
     `(org-link ((t (:foreground ,blue-1 :background ,blue-1bg :underline t :inherit nil))))
     `(org-hide ((t (:foreground ,gray-2 :background ,background))))
     `(org-date  ((t (:foreground ,purple-1 :background ,purple-1bg :underline t))))
     `(org-sexp-date ((t (:foreground ,purple-1 :background ,purple-1bg :underline t))))
     `(org-date-selected ((t (:foreground ,highlight :background ,highlight-bg))))
     `(org-done ((t (:foreground ,green-1  :background ,green-1bg   :bold nil))))
     `(org-todo ((t (:foreground ,yellow-1 :background ,yellow-1bg  :bold nil))))
     `(org-holding   ((t (:foreground ,purple-1 :background ,purple-1bg :bold nil))))
     `(org-ongoing   ((t (:foreground ,red-1 :background ,red-1bg))))
     `(org-cancelled ((t (:foreground ,comment  :background ,comment-bg  :bold nil))))
     `(org-priority-a ((t (:foreground ,red-1   :background ,red-1bg    :bold nil))))
     `(org-priority-b ((t (:foreground ,blue-1  :background ,blue-1bg   :bold nil))))
     `(org-priority-c ((t (:foreground ,comment :background ,comment-bg :bold nil))))
     `(org-table     ((t (:foreground ,foreground))))
     `(org-formula   ((t (:foreground ,orange-1 :background ,orange-1bg))))
     `(org-tag       ((t (:foreground ,blue-2   :background ,blue-2bg :bold nil))))
     `(org-block-begin-line ((t (:foreground ,gray-2 :background ,comment-bg))))
     `(org-block-end-line   ((t (:foreground ,gray-2 :background ,comment-bg))))
     `(org-block            ((t (:foreground ,foreground))))
     `(org-block-background ((t (:background ,background))))
     `(org-footnote         ((t (:foreground ,orange-1 :background ,orange-1bg))))
     `(org-meta-line        ((t (:foreground ,comment  :background ,comment-bg))))
     `(org-special-keyword  ((t (:foreground ,gray-2   :background ,comment-bg))))
     `(org-clock-overlay    ((t (:foreground ,highlight  :background ,highlight-bg :bold nil))))
     `(org-agenda-dimmed-todo-face ((t (:foreground ,comment :background ,comment-bg))))
     `(org-agenda-structure    ((t (:foreground ,foreground  :background ,gray-1bg))))
     `(org-agenda-date         ((t (:foreground ,purple-1 :background ,purple-1bg :underline t))))
     `(org-agenda-date-weekend ((t (:foreground ,red-1    :background ,red-1bg    :underline t))))
     `(org-agenda-date-today   ((t (:foreground ,blue-1   :background ,blue-1bg   :underline t :bold nil :italic nil))))
     `(org-agenda-clocking     ((t (:foreground ,yellow-1 :background ,yellow-1bg))))
     `(org-scheduled            ((t (:foreground ,foreground))))
     `(org-scheduled-today      ((t (:foreground ,foreground))))
     `(org-scheduled-previously ((t (:foreground ,orange-1   :background ,orange-1bg))))
     `(org-upcoming-deadline    ((t (:foreground ,red-1      :background ,red-1bg))))
     `(org-agenda-done          ((t (:foreground ,comment    :background ,comment-bg))))
     `(org-warning              ((t (:foreground ,orange-1   :background ,orange-1bg))))
     `(org-agenda-current-time  ((t (:foreground ,foreground :background ,gray-1bg :bold nil))))
     `(org-time-grid            ((t (:foreground ,comment    :background ,comment-bg))))
     `(org-verbatim ((t (:foreground ,blue-2 :background ,blue-2bg))))
     `(org-code     ((t (:foreground ,blue-2 :background ,blue-2bg))))

     ;; Jabber
     `(jabber-title-large         ((t (:foreground ,foreground :height 1.0 :inherit default))))
     `(jabber-title-medium        ((t (:foreground ,foreground :height 1.0 :inherit default))))
     `(jabber-title-small         ((t (:foreground ,foreground :height 1.0 :inherit default))))
     `(jabber-roster-user-online  ((t (:foreground ,green-1  :background ,green-1bg  :bold nil :italic nil))))
     `(jabber-roster-user-chatty  ((t (:foreground ,green-1  :background ,green-1bg  :bold nil :italic nil))))
     `(jabber-roster-user-away    ((t (:foreground ,yellow-1 :background ,yellow-1bg :bold nil :italic nil))))
     `(jabber-roster-user-xa      ((t (:foreground ,yellow-1 :background ,yellow-1bg :bold nil :italic nil))))
     `(jabber-roster-user-dnd     ((t (:foreground ,red-1    :background ,red-1bg    :bold nil :italic nil))))
     `(jabber-roster-user-offline ((t (:foreground ,comment  :background ,comment-bg :bold nil :italic nil))))

     `(jabber-chat-prompt-local   ((t (:foreground ,yellow-1 :background ,yellow-1bg))))
     `(jabber-chat-prompt-foreign ((t (:foreground ,blue-1   :background ,blue-1bg))))
     `(jabber-chat-prompt-system  ((t (:foreground ,orange-1 :background ,orange-1bg))))
     `(jabber-chat-error          ((t (:foreground ,red-1    :background ,red-1bg))))
     `(jabber-rare-time-face      ((t (:foreground ,purple-1 :background ,purple-1bg :underline t))))

     `(jabber-activity-face          ((t (:foreground ,yellow-1 :background ,yellow-1bg))))
     `(jabber-activity-personal-face ((t (:foreground ,blue-1   :background ,blue-1bg))))

     ;; Compilation
     `(compilation-info           ((t (:foreground ,blue-1   :background ,blue-1bg :inherit nil))))
     `(compilation-error          ((t (:foreground ,red-1    :background ,red-1bg  :inherit nil))))
     `(compilation-warning        ((t (:foreground ,red-1    :background ,red-1bg  :inherit nil))))
     `(compilation-line-number    ((t (:foreground ,purple-1 :background ,purple-1bg))))
     `(compilation-column-number  ((t (:foreground ,purple-1 :background ,purple-1bg))))
     `(compilation-mode-line-exit ((t (:foreground ,green-1  :background ,green-1bg  :bold nil :inherit nil))))
     `(compilation-mode-line-fail ((t (:foreground ,red-1    :background ,red-1bg    :bold nil :inherit nil))))
     `(compilation-mode-line-run  ((t (:foreground ,yellow-1 :background ,yellow-1bg :bold nil :inherit nil))))

     ;; Comint
     `(comint-highlight-prompt ((t (:foreground ,yellow-1 :background ,yellow-1bg :bold nil))))

     ;; Ediff
     `(ediff-current-diff-A ((t (:foreground ,foreground :background ,red-1bg))))
     `(ediff-current-diff-B ((t (:foreground ,foreground :background ,green-1bg))))
     `(ediff-odd-diff-A     ((t (:foreground ,foreground :background ,gray-3bg))))
     `(ediff-odd-diff-B     ((t (:foreground ,foreground :background ,gray-3bg))))
     `(ediff-even-diff-A    ((t (:foreground ,foreground :background ,gray-3bg))))
     `(ediff-even-diff-B    ((t (:foreground ,foreground :background ,gray-3bg))))

     ;; whitespace-mode
     `(whitespace-empty           ((t (:foreground ,yellow-1bg :background ,yellow-1))))
     `(whitespace-hspace          ((t (:foreground ,gray-2 :background ,background))))
     `(whitespace-indentation     ((t (:foreground ,gray-2 :background ,background))))
     `(whitespace-line            ((t (:background ,gray-2 :background ,background))))
     `(whitespace-newline         ((t (:foreground ,gray-2 :background ,background))))
     `(whitespace-space           ((t (:foreground ,gray-2 :background ,background))))
     `(whitespace-space-after-tab ((t (:foreground ,gray-2 :background ,background))))
     `(whitespace-tab             ((t (:foreground ,gray-2 :background ,background))))
     `(whitespace-trailing        ((t (:foreground ,red-1bg :background ,red-1))))

     ;; Gnus
     `(gnus-server-opened  ((t (:foreground ,green-1  :background ,green-1bg))))
     `(gnus-server-closed  ((t (:foreground ,purple-1 :background ,purple-1bg))))
     `(gnus-server-offline ((t (:foreground ,comment  :background ,comment-bg))))
     `(gnus-server-denied  ((t (:foreground ,red-1    :background ,red-1bg))))
     `(gnus-server-agent   ((t (:foreground ,yellow-1 :background ,yellow-1bg))))

     `(gnus-group-mail-3-empty   ((t (:foreground ,foreground :background ,background :bold nil))))
     `(gnus-group-mail-3         ((t (:foreground ,foreground :background ,background :bold nil))))
     `(gnus-group-news-3-empty   ((t (:foreground ,foreground :background ,background :bold nil))))
     `(gnus-group-news-3         ((t (:foreground ,foreground :background ,background :bold nil))))
     `(gnus-group-mail-low       ((t (:foreground ,gray-2))))
     `(gnus-group-mail-low-empty ((t (:foreground ,gray-2))))

     `(gnus-summary-selected       ((t (:foreground nil  :background ,hlline :bold nil :underline nil))))
     `(gnus-summary-normal-read    ((t (:foreground ,foreground :background ,background   :bold nil))))
     `(gnus-summary-normal-ticked  ((t (:foreground ,yellow-1   :background ,yellow-1bg))))
     `(gnus-summary-normal-ancient ((t (:foreground ,foreground :background ,background))))
     `(gnus-summary-cancelled      ((t (:foreground ,red-1      :background ,red-1bg))))

     `(gnus-header-name       ((t (:foreground ,comment :background ,comment-bg))))
     `(gnus-header-from       ((t (:foreground ,blue-1  :background ,blue-1bg))))
     `(gnus-header-subject    ((t (:foreground ,foreground :background ,gray-3bg :bold t))))
     `(gnus-header-content    ((t (:foreground ,yellow-1   :background ,yellow-1bg))))
     `(gnus-header-newsgroups ((t (:foreground ,orange-1   :background ,orange-1bg))))
     `(gnus-button            ((t (:weight normal))))
     `(widget-button          ((t (:foreground ,blue-1 :background ,blue-1bg :weight normal))))

     `(gnus-cite-1 ((t (:foreground ,blue-1   :background ,blue-1bg))))
     `(gnus-cite-2 ((t (:foreground ,purple-1 :background ,purple-1bg))))
     `(gnus-cite-3 ((t (:foreground ,red-1    :background ,red-1bg))))
     `(gnus-cite-4 ((t (:foreground ,brown-1  :background ,brown-1bg))))
     `(gnus-cite-5 ((t (:foreground ,yellow-1 :background ,yellow-1bg))))
     `(gnus-signature ((t (:background ,comment :background ,comment-bg))))

     ;; Message
     `(message-header-name       ((t (:foreground ,comment    :background ,comment-bg))))
     `(message-header-from       ((t (:foreground ,blue-1     :background ,blue-1bg))))
     `(message-header-other      ((t (:foreground ,blue-1     :background ,blue-1bg))))
     `(message-header-xheader    ((t (:foreground ,blue-1     :background ,blue-1bg))))
     `(message-header-cc         ((t (:foreground ,purple-1   :background ,purple-1bg))))
     `(message-header-subject    ((t (:foreground ,foreground :background ,gray-3bg :bold t))))
     `(message-header-content    ((t (:foreground ,yellow-1   :background ,yellow-1bg))))
     `(message-header-to         ((t (:foreground ,yellow-1   :background ,yellow-1bg))))
     `(message-header-newsgroups ((t (:foreground ,orange-1   :background ,orange-1bg))))
     `(message-separator         ((t (:foreground ,comment    :background ,comment-bg))))
     `(message-mml               ((t (:foreground ,blue-1     :background ,blue-1bg :underline t))))

     ;; Dired
     `(dired-header     ((t (:foreground ,purple-1  :background ,purple-1bg))))
     `(dired-directory  ((t (:foreground ,purple-1  :background ,purple-1bg))))
     `(dired-symlink    ((t (:foreground ,blue-1    :background ,blue-1bg :underline t))))
     `(dired-mark       ((t (:foreground ,red-1     :background ,red-1bg))))
     `(dired-marked     ((t (:foreground ,highlight :background ,highlight-bg))))
     `(dired-flagged    ((t (:foreground ,highlight :background ,highlight-bg))))
     `(dired-perm-write ((t (:foreground ,green-1   :background ,green-1bg))))

     ;; ERC
     `(erc-error-face  ((t (:foreground ,red-1   :background ,red-1bg))))
     `(erc-notice-face ((t (:foreground ,comment :background ,comment-bg))))
     `(erc-button      ((t (:foreground ,blue-1  :background ,blue-1bg :bold nil :underline t))))
     `(erc-command-indicator-face ((t (:foreground ,yellow-1 :background ,yellow-1bg :bold nil))))
     `(erc-prompt-face       ((t (:foreground ,yellow-1 :background ,yellow-1bg))))
     `(erc-current-nick-face ((t (:foreground ,red-1    :background ,red-1bg))))
     `(erc-my-nick-face      ((t (:foreground ,red-1    :background ,red-1bg))))
     `(erc-nick-default-face ((t (:foreground nil       :background nil))))
     `(erc-nick-msg-face     ((t (:foreground ,yellow-1 :background ,yellow-1bg))))
     `(erc-direct-msg-face   ((t (:foreground ,foreground))))
     `(erc-timestamp-face    ((t (:foreground ,purple-1 :background ,purple-1bg :underline t))))
     `(erc-input-face        ((t (:foreground ,foreground))))

     `(erc-nick-1-face ((t (:foreground ,red-1    :background ,red-1bg))))
     `(erc-nick-2-face ((t (:foreground ,orange-1 :background ,orange-1bg))))
     `(erc-nick-3-face ((t (:foreground ,yellow-1 :background ,yellow-1bg))))
     `(erc-nick-4-face ((t (:foreground ,green-1  :background ,green-1bg))))
     `(erc-nick-5-face ((t (:foreground ,blue-1   :background ,blue-1bg))))
     `(erc-nick-6-face ((t (:foreground ,purple-1 :background ,purple-1bg))))
     `(erc-nick-7-face ((t (:foreground ,blue-2   :background ,blue-2bg))))

     `(reb-match-0 ((t (:foreground ,yellow-1 :background ,yellow-1bg))))
     `(reb-match-1 ((t (:foreground ,orange-1 :background ,orange-1bg))))

     `(rainbow-delimiters-depth-1-face ((t (:foreground ,foreground :background ,background))))
     `(rainbow-delimiters-depth-2-face ((t (:foreground ,purple-1 :background ,purple-1bg))))
     `(rainbow-delimiters-depth-3-face ((t (:foreground ,blue-1   :background ,blue-1bg))))
     `(rainbow-delimiters-depth-4-face ((t (:foreground ,blue-2   :background ,blue-2bg))))
     `(rainbow-delimiters-depth-5-face ((t (:foreground ,green-1  :background ,green-1bg))))
     `(rainbow-delimiters-depth-6-face ((t (:foreground ,yellow-1 :background ,yellow-1bg))))
     `(rainbow-delimiters-depth-7-face ((t (:foreground ,orange-1 :background ,orange-1bg))))
     `(rainbow-delimiters-depth-8-face ((t (:foreground ,red-1    :background ,red-1bg))))
     `(rainbow-delimiters-unmatched-face ((t (:foreground ,red-2bg :background ,red-1))))

     `(yas-field-highlight-face ((t (:foreground ,highlight :background ,highlight-bg))))

     `(markdown-link-face ((t (:foreground ,foreground :background ,background :italic t :underline t))))
     `(markdown-url-face  ((t (:foreground ,blue-1 :background ,blue-1bg :underline t))))

     `(flycheck-fringe-error   ((t (:foreground ,red-1bg    :background ,red-1 :bold nil))))
     `(flycheck-fringe-warning ((t (:foreground ,orange-1bg :background ,orange-1 :bold nil))))

     `(haskell-interactive-face-prompt ((t (:foreground ,yellow-1   :background ,yellow-1bg))))
     `(haskell-interactive-face-result ((t (:foreground ,foreground :background ,background :bold t))))

     `(ein:cell-heading-1    ((t (:foreground ,foreground :background ,background :bold nil :height 2.25))))
     `(ein:cell-heading-2    ((t (:foreground ,foreground :background ,background :bold nil :height 1.50))))
     `(ein:cell-input-prompt ((t (:foreground ,yellow-1   :background ,yellow-1bg))))
     `(ein:cell-input-area   ((t nil)))

     `(markdown-header-face ((t (:foreground ,foreground :background ,background :inherit none))))
     `(markdown-header-face-1 ((t (:height 2.25))))
     `(markdown-header-face-2 ((t (:height 1.50))))
     `(markdown-header-delimiter-face ((t (:inherit none :foreground ,comment :background ,comment-bg))))
     `(markdown-italic-face ((t (:foreground ,foreground :background ,background :italic t :inherit none))))
     `(markdown-bold-face   ((t (:foreground ,foreground :background ,background :bold t   :inherit none))))

     `(company-scrollbar-bg ((t (:foreground nil :background ,modelineia-bg))))
     `(company-scrollbar-fg ((t (:foreground nil :background ,modelineia))))
     `(company-tooltip ((t (:foreground nil :background ,region :italic t))))
     `(company-tooltip-selection ((t (:foreground ,highlight :background ,highlight-bg))))
     `(company-tooltip-common ((t (:italic nil))))
     `(company-tooltip-annotation ((t (:foreground ,comment))))
     `(company-preview ((t (:foreground ,comment :background ,comment-bg :italic t))))
     `(company-preview-common ((t (:foreground nil :background nil))))

     `(antlr-keyword ((t (:inherit font-lock-keyword-face))))
     `(antlr-literal ((t (:inherit font-lock-constant-face))))
     `(antlr-ruledef ((t (:inherit font-lock-function-name-face))))
     `(antlr-ruleref ((t (:inherit font-lock-function-name-face))))
     `(antlr-tokendef ((t (:inherit font-lock-type-face))))
     `(antlr-tokenref ((t (:inherit font-lock-type-face))))
     )

    (custom-theme-set-variables
     theme
     `(imaxima-equation-color ,foreground)
     `(imaxima-label-color ,blue-1))

    ))

(provide 'twilight-definitions)
