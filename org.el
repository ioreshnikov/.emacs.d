(require 'org)
(require 'ox-md)


(add-hook 'org-mode-hook 'auto-fill-mode)
(add-hook 'org-mode-hook 'flyspell-mode)
(add-hook 'org-finish-hook
          (lambda ()
            (message "Note stored. Saving file %s"
                     (buffer-file-name))
            (save-buffer)))

(setq org-return-follows-link       t)
(setq org-hide-leading-stars        t)
(setq org-odd-levels-only           t)
(setq org-special-ctrl-a/e          t)
(setq org-tags-column             -120)
(setq org-src-fontify-natively      t)
(setq org-log-states-order-reversed t)
(setq org-log-into-drawer           t)

(setq org-directory "~/Dropbox/Notes/")
(setq org-agenda-files     (concat org-directory ".Agenda"))
(setq org-archive-location (concat org-directory ".Archive/%s::"))
(setq org-agenda-ndays 1)

(setq org-todo-keywords '((sequence "TODO(t)"
                                    "LIVE(l@/@)"
                                    "HOLD(h@/@)"
                                    "|"
                                    "DONE(d@/@)"
                                    "ABRT(a@/@)")))

(defface org-holding
  '((t (:foreground "orange" :background nil :bold nil)))
  "Face to highlight org-mode TODO keywords for delayed tasks."
  :group 'org-faces)

(defface org-ongoing
  '((t (:foreground "orange" :background nil :bold nil)))
  "Face to highlight org-mode MOVE keywords for delegated tasks."
  :group 'org-faces)

(defface org-cancelled
  '((t (:foreground "red" :background nil :bold nil)))
  "Face to highlight org-mode TODO keywords for cancelled tasks."
  :group 'org-faces)

(setq org-todo-keyword-faces '(("TODO" . org-todo)
                               ("LIVE" . org-ongoing)
                               ("HOLD" . org-holding)
                               ("DONE" . org-done)
                               ("ABRT" . org-cancelled)))

(defface org-priority-a
  '((t (:foreground "red" :background nil :bold nil)))
  "Face to highlight org-mode priority #A"
  :group 'org-faces)

(defface org-priority-b
  '((t (:foreground "yellow" :background nil :bold nil)))
  "Face to highlight org-mode priority #B"
  :group 'org-faces)

(defface org-priority-c
  '((t (:foreground "green" :background nil :bold nil)))
  "Face to highlight org-mode priority #C"
  :group 'org-faces)

(setq org-priority-faces '((?A . org-priority-a)
                           (?B . org-priority-b)
                           (?C . org-priority-c)))

(setq org-tag-alist '())

;; Export
(setq org-export-backends '(ascii
                            beamer
                            html
                            latex
                            md
                            odt))

(setq org-format-latex-options
      '(:foreground default
        :background default
        :scale 1.50
        :html-foreground "Black"
        :html-background "Transparent" :html-scale 1.0
        :matchers ("begin" "$1" "$" "$$" "\\(" "\\[")))

(setq org-export-latex-todo-keyword-markup
      '(("TODO" . "\\todo")
        ("HOLD" . "\\hold")
        ("DONE" . "\\done")
        ("ABRT" . "\\abrt")))

(setq org-export-date-timestamp-format "%d %B %Y")

(setq org-export-html-preamble  nil)
(setq org-export-html-preamble-format
      `(("en" ,(concat "<span class=\"author\">%a</span>"
                       "<span class=\"email\">%e</span>"
                       "<span class=\"date\"%d</span>"))))

(setq org-export-html-postamble t)
(setq org-export-html-postamble-format
      `(("en" ,(concat "<span class=\"author\">%a</span><br/>"
                       "<span class=\"email\">%e</span><br/>"
                       "<span class=\"date\">%d</span>"))))


;; Capture
(setq org-capture-templates
      `(("t" "General task"
             entry
             (file (concat org-directory "Unsorted.org"))
             "* TODO %? :task:\n"
             :empty-lines 1)
        ("m" "meeting"
             entry
             (file (concat org-directory "Unsorted.org"))
             "* TODO %? :appointment:\n"
             :empty-lines 1)))

;; Refile
(setq org-refile-targets
      `((nil :maxlevel . 3)
        (org-agenda-files :maxlevel . 3)))
(setq org-refile-use-outline-path t)
(setq org-outline-path-complete-in-steps t)
