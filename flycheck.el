(require 'flycheck)

(setq flycheck-check-syntax-automatically '(save mode-enabled))
(setq flycheck-indication-mode 'left-fringe)
