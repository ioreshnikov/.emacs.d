(require 'haskell-mode)

(add-hook 'haskell-mode-hook 'interactive-haskell-mode)
(add-hook 'haskell-mode-hook 'haskell-indent-mode)
(add-hook 'haskell-mode-hook 'haskell-doc-mode)
(add-hook 'haskell-mode-hook (lambda () (run-hooks 'prog-mode-hook)))
(setq haskell-program-name "ghci")

(add-hook 'flycheck-mode-hook #'flycheck-haskell-setup)
