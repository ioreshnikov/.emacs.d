(require 'magit)


(setq magit-status-buffer-switch-function 'switch-to-buffer)
(setq magit-status-insert-sections-hook
      '(magit-insert-status-local-line
        magit-insert-status-remote-line
        magit-insert-status-head-line
        magit-insert-status-tags-line
        magit-insert-status-merge-line
        magit-insert-status-rebase-lines
        magit-insert-empty-line
        magit-insert-rebase-sequence
        magit-insert-bisect-output magit-insert-bisect-rest
        magit-insert-bisect-log
        magit-insert-pending-changes
        magit-insert-pending-commits
        magit-insert-staged-changes
        magit-insert-unstaged-changes
        magit-insert-unpulled-commits
        magit-insert-unpushed-commits
        magit-insert-stashes
        magit-insert-untracked-files))
