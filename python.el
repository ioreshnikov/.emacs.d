(require 'python)
(require 'flycheck-pyflakes)
(require 'anaconda-mode)

(setq python-shell-interpreter "/usr/bin/ipython3")
(setq gud-pdb-command-name "python3 -m pdb")

(add-hook 'python-mode-hook 'flycheck-mode)
(add-hook 'python-mode-hook 'anaconda-mode)

(eval-after-load "company"
 '(add-to-list 'company-backends 'company-anaconda))
