(add-to-list 'load-path              "~/.emacs.d/themes/twilight")
(add-to-list 'custom-theme-load-path "~/.emacs.d/themes/twilight")
(add-to-list 'load-path              "~/.emacs.d/themes/solarized")
(add-to-list 'custom-theme-load-path "~/.emacs.d/themes/solarized")

;; Common appearance settings
(setq inhibit-startup-message t)
(setq use-dialog-box nil)
(setq resize-mini-windows nil)
(setq fringe-indicator-alist nil)
(setq-default cursor-in-non-selected-windows nil)
(setq-default line-spacing 3)

;; Frame settings
(setq default-frame-alist `((font . "CMU Typewriter Text 13")
                            (menu-bar-lines -1)
                            (tool-bar-lines -1)
                            (vertical-scroll-bars . nil)))

(setq frame-title-format  '(buffer-file-name "%f" ("%b")))

;; Display-modes setup
(tooltip-mode -1)
(setq linum-format " %3d ")
(column-number-mode t)
(show-paren-mode t)
(blink-cursor-mode nil)
