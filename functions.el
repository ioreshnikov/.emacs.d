;; Buffers
(defun kill-all-buffers ()
  "Kill all buffers without prompting."
  (interactive)
  (let ((list (buffer-list)))
    (while list
      (let* (buffer (car list))
        (kill-buffer buffer))
      (setq list (cdr list)))))

(defun rename-file-and-buffer (new-name)
  "Renames both current buffer and file it's visiting to NEW-NAME." (interactive "sNew name: ")
  (let ((name (buffer-name))
        (filename (buffer-file-name)))
    (if (not filename)
        (message "Buffer '%s' is not visiting a file!" name)
      (if (get-buffer new-name)
          (message "A buffer named '%s' already exists!" new-name)
        (progn       (rename-file name new-name 1)
                 (rename-buffer new-name)
                 (set-visited-file-name new-name)
                 (set-buffer-modified-p nil))))))

(defun switch-to-scratch ()
  (interactive)
  (let ((scratch   "*scratch*")
        (was-there nil))
    (setq was-there (get-buffer scratch))
    (switch-to-buffer scratch)
    (unless was-there
      (insert initial-scratch-message))))

;; Comment Annotations
(defun font-lock-comment-annotations ()
  (interactive)
  (font-lock-add-keywords
   nil '(("\\<\\(\\(?:BUG\\|FIXME\\|HACK\\|MAGIC\\|TODO\\|XXX\\):\\)" 1 font-lock-warning-face t))))

;; Open eshell in the directory of the current file.
(defun eshell-open-here ()
  (interactive)
  (let ((this-buffer (current-buffer))
        (directory   default-directory))
    (eshell)
    (switch-to-buffer this-buffer)
    (switch-to-buffer-other-window eshell-buffer-name)
    (end-of-buffer)
    (unless (string= directory default-directory)
      (cd directory)
      (eshell-send-input)
      (end-of-buffer))))
