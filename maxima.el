(add-to-list 'load-path "/usr/share/maxima/5.35.1/emacs")

(require 'maxima)
(require 'imaxima)

;; Imaxima scaling
(setq imaxima-scale-factor 1.35)

;; Recognize .mac and .max files as a maxima files.
(add-to-list 'auto-mode-alist '("\\.ma[cx]\\'" . maxima-mode))

(setq maxima-use-full-color-in-process-buffer t)
(setq imaxima-use-maxima-mode-flag t)
