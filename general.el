;; General settings
(setq user-full-name    "Ivan Oreshnikov")
(setq user-mail-address "oreshnikov.ivan@gmail.com")

;; Be completely silent
(setq ring-bell-function 'ignore)

;; Language
(setq ispell-dictionary "english")

;; Tabs
(setq-default truncate-lines t)
(setq-default tab-width 4)
(setq-default indent-tabs-mode nil)
(setq-default c-default-style "stroustrup")
(setq tab-stop-list (number-sequence 4 120 4))

;; Backup and auto-save
(setq backup-inhibited t)
(setq auto-save-default nil)

(setq mouse-yank-at-point t)
(setq scroll-error-top-bottom t)
(setq woman-use-own-frame nil)
(setq calendar-week-start-day 1)
(setq sentence-end-double-space nil)
(setq default-input-method "TeX")
(setq reb-re-syntax 'rx)
(setq echo-keystrokes 0.001)
(setq show-paren-delay 0.0)

(defalias 'yes-or-no-p 'y-or-n-p)

;; Subword mode
(global-subword-mode)

;; Auto revert
(global-auto-revert-mode 1)

;; Saner selection
(setq delete-active-region 'kill)
(setq delete-selection-mode t)

;; Ido-mode setup
(require 'ido)
(ido-mode t)
(ido-everywhere t)
(setq ido-enable-flex-matching t)
(setq ido-default-file-method   'selected-window
      ido-default-buffer-method 'selected-window
      iswitchb-default-method   'samewindow)

;; Dired setup
(require 'dired-x)
(setq dired-listing-switches "-lGXh --group-directories-first")
(setq dired-omit-files (concat dired-omit-files "\\|^\\..+$"))
(add-hook 'dired-mode-hook 'dired-omit-mode)

;; Uniquify buffer names
(require 'uniquify)
(setq uniquify-buffer-name-style 'forward)

;; Browser
(setq browse-url-generic-program  "google-chrome-stable")
(setq browse-url-browser-function 'browse-url-generic)

;; Info reader
(setq Info-use-header-line nil)

;; Electric pairs
(electric-pair-mode 1)

;; Electric indent
(electric-indent-mode 0)

;; Prog-mode
(add-hook 'prog-mode-hook
          '(lambda ()
             (ispell-change-dictionary "en")
             (flyspell-prog-mode)))
(add-hook 'prog-mode-hook
          '(lambda ()
             (add-hook 'before-save-hook 'delete-trailing-whitespace)))
(add-hook 'prog-mode-hook 'linum-mode)
(add-hook 'prog-mode-hook 'font-lock-comment-annotations)
(add-hook 'prog-mode-hook 'rainbow-delimiters-mode)

;; Focus mode
(require 'focus-mode)
(setq focus-working-area-width 680)
(setq focus-hide-faces (append focus-hide-faces '(linum)))

;; Whitespace mode
(setq whitespace-style '(face spaces tabs newline
                         tab-mark space-mark newline-mark))

(setq whitespace-display-mappings '((space-mark 32   [183] [46])
                                    (space-mark 160  [183] [46])
                                    (space-mark 2208 [183] [46])
                                    (space-mark 2336 [183] [46])
                                    (space-mark 3616 [183] [46])
                                    (space-mark 3872 [183] [46])
                                    (newline-mark 10 [172 10])
                                    (tab-mark 9 [187 9] [92 9])))

;; Saner ediff
(setq ediff-diff-options "-w")
(setq ediff-split-window-function 'split-window-horizontally)
(setq ediff-window-setup-function 'ediff-setup-windows-plain)

;; Enable upcase-region and such.
(put 'upcase-region     'disabled nil)
(put 'capitalize-region 'disabled nil)
(put 'downcase-region   'disabled nil)
