;; Global key bindings

;; Buffer and file related bindings
(global-set-key (kbd "C-c b") 'switch-to-scratch)

;; Display related bindings
(global-set-key (kbd "<f9>")  'linum-mode)
(global-set-key (kbd "<f10>") 'whitespace-mode)
(global-set-key (kbd "<f11>") 'toggle-frame-fullscreen)
(global-set-key (kbd "<f12>") 'focus-mode)

;; Window related bindings
(global-set-key (kbd "M-1") 'delete-other-windows)
(global-set-key (kbd "M-2") 'split-window-vertically)
(global-set-key (kbd "M-3") 'split-window-horizontally)
(global-set-key (kbd "M-0") 'delete-window)
(global-set-key (kbd "M-o") 'other-window)
(global-set-key (kbd "C-M-o") (lambda () (interactive) (other-window -1)))

;; Org-mode
(global-set-key (kbd "C-c a") 'org-agenda)
(global-set-key (kbd "C-c c") 'org-capture)
(global-set-key (kbd "C-c s") 'org-store-link)

;; Text scale (grabbed from starter-kit)
(global-set-key (kbd "C-+") 'text-scale-increase)
(global-set-key (kbd "C--") 'text-scale-decrease)
(global-set-key (kbd "C-_") 'text-scale-decrease) ;; For sake of consistency.

;; Text-editing tasks.
(global-set-key (kbd "C-M-h") 'backward-kill-word)
(global-set-key (kbd "C-h")   'delete-backward-char)
(global-set-key (kbd "M-?")   'help-command)
(global-set-key (kbd "M-k") (lambda () (interactive) (join-line)))
(global-set-key (kbd "M-j") (lambda () (interactive) (join-line -1)))
(global-set-key (kbd "M-a") 'align)

;; Emoticons!
(global-set-key (kbd "C-9") (lambda () (interactive) (insert ":(")))
(global-set-key (kbd "C-0") (lambda () (interactive) (insert ":)")))

;; Terminal
(global-set-key (kbd "C-c t") 'eshell-open-here)

;; Magit
(global-set-key (kbd "C-c g") 'magit-status)
