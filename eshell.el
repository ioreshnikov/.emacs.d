(require 'eshell)

(setq eshell-scroll-to-bottom-on-input t)
(setq eshell-scroll-to-bottom-on-output t)

(setq eshell-save-history-on-exit t)
(setq eshell-hist-ignoredups t)

(setq eshell-banner-message "")


(setq eshell-prompt-regexp "^[^#$\n]* [#%] ")
(setq eshell-prompt-function
      (lambda nil
        (concat (abbreviate-file-name (eshell/pwd))
                (if (= (user-uid) 0)
                    " # "
                    " % "))))

(add-hook 'eshell-mode-hook
          '(lambda () (setq pcomplete-cycle-completions nil)))
